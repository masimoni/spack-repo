##############################################################################
from spack import *


class PyGenfem(PythonPackage):
    """A fast and easy way to generate finite element matrices"""

    homepage = "https://gitlab.inria.fr/solverstack/genfem"
    url = "https://gitlab.inria.fr/solverstack/genfem.git"
    git = "https://gitlab.inria.fr/solverstack/genfem.git"

    import_modules = ['genfem']

    version('git',  branch='master')
    version('1.0', tag='1.0')
    version('1.1', tag='1.1', preferred=True)

    extends('python', ignore=r'bin/pytest')

    depends_on('py-numpy')
    depends_on('py-scipy')
    depends_on('py-sympy')
    depends_on('py-setuptools')
